<?php 
/*
*Este script toma un conjunto de objetos importados para generar e insertar
*el IDNO, preferred label, relaciones y crear un espectáculo relacionado en 
*caso de que no exista
*/

//Consantes propias
define('WEBROOT', $_SERVER['DOCUMENT_ROOT']);
define('CDTC_NAME','cdtc-beta');
define('APP_PATH',WEBROOT.'/'.CDTC_NAME);
define('SERVER_IP','192.168.78.7');

//Archivos requeridos
require_once('setup.php');
require_once(__CA_LIB_DIR__.'/ca/Search/ObjectSearch.php');
require_once(__CA_MODELS_DIR__.'/ca_objects.php');
require_once(__CA_MODELS_DIR__.'/ca_object_labels.php');
require_once(__CA_MODELS_DIR__.'/ca_occurrences.php');
require_once(__CA_MODELS_DIR__.'/ca_objects_x_occurrences.php');

####
#DB#
####
//Datos de la base de datos de providence
$DBServer = 'localhost'; // 'localhost' o una ip, por ej '192.168.1.100'
$DBUser   = 'dbuser';
$DBPass   = 'dbpasswd';
$DBName   = 'dbname';

//Generamos un objeto de la base de datos
$conn = new mysqli($DBServer, $DBUser, $DBPass, $DBName);

// chequeamos la conexión
if ($conn->connect_error) {
  trigger_error('Database connection failed: '  . $conn->connect_error, E_USER_ERROR);
}
#mysqli_set_charset($conn,"utf8");
$conn->set_charset("utf8");

###########
#FUNCIONES#
###########

//Función para generar el nuevo identificador objeto
function mangle_idno($idno){
	$tmp = str_replace('.pdf','', $idno);
	$tmp2 = explode('-', $tmp);
	$year = $tmp2[0];
	$text =  $tmp2[1];
	$new_idno = 'PROG_'.$text.'_'.$year;
	return $new_idno;
}

//Función para generar el nuevo título objeto
function mangle_title($idno){
	$tmp = str_replace('PROG_','', $idno);
	$year = str_replace('_', '', substr($tmp, -5));
	$tmp_year = '_'.$year;
	$text = str_replace('', '_', str_replace($tmp_year, '', $tmp)) ;
	$new_title =  ucfirst(trim(strtolower($text.' ['.$year.']')));
	return $new_title;
}

//Función para extraer el año
function extract_year($idno){
	$year = substr($idno, 0, 4);
	return $year;
}

//Función para extraer el label_id del objeto
function extract_object_label_id($object_id,$conn){
	$sql = "SELECT la.label_id FROM ca_object_labels la, ca_objects obj WHERE la.object_id = ? AND obj.deleted = 0";
 		
	// Prepare statement 
	$stmt = $conn->prepare($sql);
	if($stmt === false) {
  		trigger_error('Wrong SQL: ' . $sql . ' Error ' . $conn->error, E_USER_ERROR);
	}
 	// Bind parameters. TYpes: s = string, i = integer, d = double,  b = blob 
	$stmt->bind_param('i',$object_id);
	
	// Execute statement
	$stmt->execute();

	$stmt->bind_result($db_id);
	while ($stmt->fetch()) {
  		$db_id = $db_id;
	}
   	if (!empty($db_id) AND isset($db_id)) {
   		return $db_id;
 	 }
}

//Función para extraer el label_id del objeto
function update_object_label($label_id,$obj_title,$conn){
	$sql = "UPDATE ca_object_labels SET 'locale_id' = 2, 'is_preferred' = 1, name = ? WHERE label_id = ";

	// Prepare statement 
	$stmt = $conn->prepare($sql);
	if($stmt === false) {
  		trigger_error('Wrong SQL: ' . $sql . ' Error ' . $conn->error, E_USER_ERROR);
	}
 	// Bind parameters. TYpes: s = string, i = integer, d = double,  b = blob 
	$stmt->bind_param('is',$label_id,$obj_title);
	
	// Execute statement
	$stmt->execute();

	$stmt->close();
}

//Función para extraer el label_id de la ocurrencia
function occurrenceExists($label,$conn){
	$sql = "SELECT la.occurrence_id FROM ca_occurrence_labels la, ca_occurrences occ WHERE la.name = ? AND occ.deleted = 0";
 		
	// Prepare statement 
	$stmt = $conn->prepare($sql);
	if($stmt === false) {
  		trigger_error('Wrong SQL: ' . $sql . ' Error en occurrenceExists2: ' . $conn->error, E_USER_ERROR);
	}
 	// Bind parameters. TYpes: s = string, i = integer, d = double,  b = blob 
	$stmt->bind_param('s',$label);
	
	// Execute statement
	$stmt->execute();

	$stmt->bind_result($db_id);
	while ($stmt->fetch()) {
  		$db_id = $db_id;
	}
   	if (!empty($db_id) AND isset($db_id)) {
   		return $db_id;
 	 }
}

//Función para extraer el label_id de la ocurrencia
function cleanTitleForSearch($label){
	for ($i=1; $i < 10; $i++) { 
		$tmp_number = ' 00'.$i; 
		if (strpos($label, $tmp_number) != false) {
			$clean_title = str_replace($tmp_number, '', $label);
			return $clean_title;
		}

	}
	for ($i=10; $i < 20; $i++) { 
		$tmp_number = ' 0'.$i; 
		if (strpos($label, $tmp_number) != false) {
			$clean_title = str_replace($tmp_number, '', $label);
			return $clean_title;
		}
	}
	if (strpos($label,' 0') === false) {
		return $label;
	}
}

function nombreMedia($object_id){
	//Datos usuario de providence
	$user = 'providence_user';/
	$passwd = 'providence_passwd';
	$ip = 'providence_ip';
	
	//Setup cURL
	$url = "http://$user:$passwd@$ip/cdtc-beta/service.php/item/ca_objects/id/$object_id";
	$postData = json_encode(['bundles'=>['ca_object_representations.original_filename'=>["returnURL" => "true"]]],true);
	$ch = curl_init($url);
	curl_setopt_array($ch, array(
    	CURLOPT_POST => TRUE,
    	CURLOPT_RETURNTRANSFER => TRUE,
    	CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
    ),
    CURLOPT_POSTFIELDS => $postData
	));

	$response  = curl_exec($ch);
	curl_close($ch);
}

function buildHtml($log){
	$file = 'logHtml-' . $log['comienzo'] . 'html';
	file_put_contents($file, 
	'<!DOCTYPE html>
	<html>
		<head>
			<title>LOG ' . $comienzo . '</title>
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.2.3/foundation-flex.min.css">
		</head>

		<body>');
	file_put_contents($file, '<div class"start">LOG ' . $log['comienzo'] . '</div>', FILE_APPEND | LOCK_EX);
	file_put_contents($file, "</body>" . "<br>", FILE_APPEND | LOCK_EX);
	file_put_contents($file, "</html>" . "<br>", FILE_APPEND | LOCK_EX);
}

/*Log
*Escribir los contenidos en el fichero,
*usando la bandera FILE_APPEND para añadir el contenido al final del fichero
*y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
*/ 
$comienzo = date('d/m/Y h:i:s');
$file = 'logIdno.html';
$log['comienzo'] = date('d/m/Y h:i:s');

file_put_contents($file, 
'<!DOCTYPE html><html><head><title>LOG ' . $comienzo . '</title><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.2.3/foundation-flex.min.css">
</head><body>');

file_put_contents($file, '<div class="panel callout radius">' . "<br>", FILE_APPEND | LOCK_EX);
file_put_contents($file, "<H1>LOG</H1> $comienzo" . "<br>", FILE_APPEND | LOCK_EX);

//1 Escribir, 0 Prueba
$write = 0;

//Locale español
$pn_locale_id = 2;

//
//Comentados por seguridad
//$min = 11993; //min object_id 
//$max = 20767; //max object_id
$i = $min;
$contador = 1;
$total = $max - $min;
file_put_contents($file, "<br>" . "$total registros" . "<br>" . "<br>", FILE_APPEND | LOCK_EX);
$log['total_registros'] = $total;
$log['id_min'] = $min;
$log['id_max'] = $max;
file_put_contents($file, '</div>' . "<br>", FILE_APPEND | LOCK_EX);

while ( $i <= $max ) {
	if ( $write == 0 AND $contador == 10 ) {
		exit('terminados primeros 10');
	}

	$value = $i;

	file_put_contents($file, '<div class="panel">' . "<br>", FILE_APPEND | LOCK_EX);

	file_put_contents($file, "Object id : $value ($contador de $total)" . "<br>", FILE_APPEND | LOCK_EX);
	$log['object_id'] = $value;

	//Instanciación de cada objeto de tipo objeto
	$obj = new ca_objects($value);
	
	//Extracción del label_id del objeto
	$label_id = extract_object_label_id($value,$conn);
	file_put_contents($file, "Label id : $label_id" . "<br>", FILE_APPEND | LOCK_EX);
	$log[$object_id]['label_id'] = $label_id;

	//Instanciación de la etiqueta de un objeto
	$obj_label = new ca_object_labels($label_id);
	$obj_idno = $obj->get('ca_objects.preferred_labels'); //Cambiado porque el idno estaba vacío
	$old_label = $obj->get('ca_objects.preferred_labels');
	file_put_contents($file, "Label original: $old_label" . "<br>", FILE_APPEND | LOCK_EX);
	$log[$object_id]['label_original'] = $old_label;

	//Extracción año del idno crudo
	$year = extract_year($old_label);
	file_put_contents($file, "Año: $year" . "<br>", FILE_APPEND | LOCK_EX);
	$log[$object_id]['year'] = $year;

	//Generación nuevo idno
	$new_idno = str_replace(' ','_',mangle_idno($obj_idno));
	file_put_contents($file, "IDNO: $new_idno" . "<br>", FILE_APPEND | LOCK_EX);
	$log[$object_id]['new_idno'] = $new_idno;

	//Generación nuevo título
	$new_title = mangle_title($new_idno);
	$occ_title = cleanTitleForSearch(str_replace('_', ' ', $new_title));
	$obj_title = str_replace('_', ' ', $new_title);
	
	file_put_contents($file, "Label: $obj_title" . "<br>", FILE_APPEND | LOCK_EX);
	$log[$object_id]['new_label'] = $obj_title;

	#Comienzo Escritura
	if ($write == 1) {
					
		//Escritura nueva label
		$obj_label->setMode(ACCESS_WRITE);
		$obj_label->set(array('name' => $obj_title,'locale_id'=>2, 'is_preferred'=>1)); 
		$obj_label->update(); //Update 

		//Escritura nuevo identificador
		$obj->setMode(ACCESS_WRITE);//Set access mode to WRITE
		$obj->set(array('idno' => $new_idno,'locale_id'=>2));// Update some intrinsic data. 
		$obj->update(); //Update  

		//Escritura año objeto
		$obj->addAttribute(array('creation_date'=>$year ),'creation_date ');
		$obj->setMode(ACCESS_WRITE); //Set access mode to WRITE
		$obj->update(); //Update the record

		//Escritura nota de contenido 
		$obj->addAttribute(array('Notas_contenido'=>$obj_title),'Notas_contenido ');
		$obj->setMode(ACCESS_WRITE); //Set access mode to WRITE
		$obj->update(); //Update the record

		//echo "obj_title 2nd $obj_title"<br>"";
		//Escritura nota interna
		$nota = 'Título generado en el proceso de digitalización '."<br>".$obj_title;
		$obj->addAttribute(array('internal_notes'=>$nota),'internal_notes ');
		$obj->setMode(ACCESS_WRITE); //Set access mode to WRITE
		$obj->update(); //Update the record
			
		//IDNO Ocurrencia
		$idno_occ_tmp = str_replace(' ','_', $occ_title);
		$idno_occ_tmp2 = str_replace('[','', $idno_occ_tmp);
		$idno_occ = str_replace(']','', $idno_occ_tmp2);
			
		//Check occurrence exists
		$occ_exists = occurrenceExists($occ_title,$conn);
		if (!empty($occ_exists)) {
			//Crear relacion
			$rel = new ca_objects_x_occurrences();
			$rel->setMode(ACCESS_WRITE); 
			$rel->set(array('occurrence_id'=>$occ_exists,'object_id'=>$value,'type_id' => 50,'idno'=>$idno_occ));
			$rel->insert();//Insert the object
			$occ_id = $occ_exists;
			file_put_contents($file, "Ocurrencia relacionada: $occ_exists" . "<br>", FILE_APPEND | LOCK_EX);
		}
				
		elseif (empty($occ_exists)) {
			//echo "$occ_title NO existe "<br>"";
			file_put_contents($file, "No se encontró una ocurrencia relacionada" . "<br>", FILE_APPEND | LOCK_EX);
			//Crear espectáculo
			$occ = new ca_occurrences(); 
			$occ->setMode(ACCESS_WRITE); 
			$occ->set(array('type_id' => 181, 'locale_id'=>2,'access'=>0,'status'=>0,'idno'=>$idno_occ));
			$occ->insert();//Insert 
					
			//Id del nuevo espectáculo
			$occ_id=$occ->get('ca_occurrences.occurrence_id');
					
			//Guardamos las ocurrencias creadas en un array
			#$ocurrencias_creadas[] = array('id'=>$occ_id,'idno'=>$idno_occ);

			//Escritura año espectáculo
			$occ->addAttribute(array('creation_date'=>$year ),'creation_date ');
			$occ->setMode(ACCESS_WRITE); //Set access mode to WRITE
			$occ->update(); //Update the record

			//Escritura nueva etiqueta 
			$occ_label = new ca_occurrence_labels();
			$occ_label->setMode(ACCESS_WRITE);
			$occ_label->set(array('occurrence_id'=>$occ_id,'locale_id'=>2, 'name' => $occ_title,'is_preferred'=>1)); 
			$occ_label->insert();

			//Crear relacion
			$rel = new ca_objects_x_occurrences();
			$rel->setMode(ACCESS_WRITE); 
			$rel->set(array('occurrence_id'=>$occ_id,'object_id'=>$value,'type_id' => 50));
			$rel->insert();//Insert the object
		}
			
		//Instacia nueva ocurrencia
		$new_occ = new ca_occurrences($occ_id);
		$new_occ_indo = $new_occ->get('ca_occurrences.idno');
		$new_occ_label = $new_occ->get('ca_occurrences.preferred_labels');
			
		//Impresión de los valores en pantalla

		$link_objeto = '<a href="http://'.SERVER_IP.'/'.CDTC_NAME.'/index.php/editor/objects/ObjectEditor/Edit/object_id/'.$value.'">'.$new_title.'</a>';
		$link_occ = '<a href="http://'.SERVER_IP.'/'.CDTC_NAME.'/index.php/editor/occurrences/OccurrenceEditor/Edit/occurrence_id/'.$occ_id.'">'.$new_occ_label.'</a>';

		//echo "Link objeto: ". $link_objeto.""<br>"";
		//echo "Link ocurrencia: ".$link_occ .""<br>"";
		file_put_contents($file, "Link objeto $link_objeto". "<br>", FILE_APPEND | LOCK_EX);
		file_put_contents($file, "Link ocurrencia relacionada $link_occ" . "<br>", FILE_APPEND | LOCK_EX);

	}//End if write
	file_put_contents($file, "</div>", FILE_APPEND | LOCK_EX);
	file_put_contents($file, "<br>" . "<hr>" . "<br>", FILE_APPEND | LOCK_EX);
	$i++;
	$contador++;

}//End while

$finalizado = date('d/m/Y h:i:s');

$segundos = strtotime($finalizado) - strtotime($comienzo);

$duracion = 0;
if ($segundos < 60) {
	$duracion = "$segundos segundos";
}
if ( $segundos >= 60 AND $segundos < 3600 ) {
	$duracion = $segundos/60 . " minutos";
}
if ($segundos >= 3600) {
	$duracion = $segundos/60/60 . " horas";
}

$log['finalizado'] = $finalizado;
$log['segundos'] = $segundos;
$log['duracion'] = $duracion;

file_put_contents($file, "Finalizado $finalizado " . "<br>", FILE_APPEND | LOCK_EX);
file_put_contents($file, "Duración $duracion " . "<br>", FILE_APPEND | LOCK_EX);
file_put_contents($file, "</body>", FILE_APPEND | LOCK_EX);
file_put_contents($file, "</html>", FILE_APPEND | LOCK_EX);
# Providence know how

Este repositorio contiene una wiki y scripts con ejemplos de las operacionesas comunes relacionadas a Providence.

## Conocimientos mínimos 
La profundidad requerida de los mismos variará dependiendo de la tarea a ejecutar.  
  *  PHP  
  *  MySql  
  *  Http  
  *  Linux  
  *  Git  

## Clonar el repositorio
```
$ git clone https://smscarano@bitbucket.org/smscarano/providence_know_how.git/wiki
```

## Wiki
[Link a la wiki](https://bitbucket.org/smscarano/providence_know_how/wiki/Home
)